#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Hafee is a 
It is developed to be useful for students.

git repository: https://gitlab.gwdg.de/gruen-und-technik/hafee
No homepage or documentation yet.

Copyright (c) 2022, Felix Schelle.
License: AGPLv3-or-later (see LICENSE for details)
"""

__author__ = 'Felix Schelle'
__version__ = '0.1.1'
__license__ = 'AGPLv3-or-later'

import argparse
import pathlib
import json
import socket
# import time
from hashlib import blake2b

# import targets

class Hafee:
    def __init__(self, config):
        self.config = config
        self.upstream = self.config['upstream']
        self.salt = self.config['secret'].encode('utf-8')
        target = self.config['target']
        targets = {
                'ronils': self.target_ronils,
                'kdv': self.target_kdv,
            }
        if not target in targets:
            print(f"Error: Unkown target {target}. Check your config file.")
            print(f"Valid targets: {targets.keys()}")
        self.handle = targets[target]


    # === infinite loop ===

    def run(self):
        # todo check for server ready
        # initial terminal clear
        print("\033[H\033[J", end="")
        while True:
            try:
                raw = input("°-°) ")
                # 0007x1234567\LF
            except EOFError:
                raw = "x"

            if raw in ["exit", "quit", "x", "q"]:   # todo shutdown commands
                return

            data = self.handle(raw, self.salt)
            # print(f"DEBUG: data={data}")
            if data:
                if 'echo' in self.upstream:
                    self.echo(data)
                if 'file' in self.upstream:
                    self.write_file(data)
                if 'server' in self.upstream:
                    self.send_server(data)
            # clear terminal
            print("\033[H\033[J", end="")


    # === upstream handles ===

    def send_server(self, data):
        host = self.config['host']
        port = self.config['port']
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((host, port))
            s.sendall(data.encode('utf-8'))
            # s.recv(1024)
            # todo implement hafee protocol

    def write_file(self, data):
        # get file config from self.target
        print("Warning: write_file not yet implemented!")

    def echo(self, data):
        print(data)


    # === targets ===

    def target_ronils(self, data, salt):
        if len(data) < 12:
            # no valid SUB-number
            return None
        data = data [4:]
        # cut SUB-number
        # todo: test with new cards how first number looks like
        if int(data[0]) % 2:
            # wise mat value
            matnr = "1"+data[1:]
        else:
            # sose mat value
            matnr = "2"+data[1:]
        b_matnr = matnr.encode('utf-8')
        hashed_matnr = blake2b(b_matnr, digest_size=20, salt=salt).hexdigest()
        return hashed_matnr

    def target_kdv(self, data, secret):
        print("Error: target kdv not yet implemented")


def main():
    parser = argparse.ArgumentParser()
    #parser.add_argument('module')
    parser.add_argument('config', default='config/ronils_example.json')
    # parser.add_argument('-c', '--config', default='config/conf_example.json')
    parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')
    args = parser.parse_args()

    with open(args.config, 'r') as f:
        config = json.load(f)

    hafee_client = Hafee(config)
    hafee_client.run()
    
if __name__ == "__main__":
    main()
