# hafee

![Logo](logo.png)

Tool to get scanned bar-codes data and transfer it to other tools.

README will be written soon.

## Licencse

**AGPL-3.0-or-later**  
![AGPLv3](agplv3.png)  
see [License](LICENSE) for more information.  

Copyright (c) 2022, Felix Schelle.
